package no.ntnu.imt3281.records;

import java.util.HashMap;

/**
 * Class with one static method used to find url for the cover image for a given album
 * 
 * @author oivindk
 *
 */
public class FrontCoverFinder {

	/**
	 * Go through the given hash map and try to find images.x.thumbnails.large. If no images.x.large is found 
	 * then try to find images.x.thumbnails.small. If that isn't found either then look for  
	 * an images.x.image. (x is a number from 0->)  
	 * @param map
	 * @return
	 */
	public static String getCoverURL(HashMap<String, String> map) {
		if (map.containsKey("images.0.thumbnails.large"))
			return map.get("images.0.thumbnails.large");
		else if (map.containsKey("images.0.thumbnails.small"))
			return map.get("images.0.thumbnails.large");
		else if (map.containsKey("images.0.image"))
			return map.get("images.0.image");
		return "";
	}

}
