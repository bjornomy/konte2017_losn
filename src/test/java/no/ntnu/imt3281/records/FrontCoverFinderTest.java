package no.ntnu.imt3281.records;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

public class FrontCoverFinderTest {
	String json;
	HashMap<String, String> map = new HashMap<>();
	
	@Before
	public void setUp() throws Exception {
		/*
		 * json = ReadJSON.read("http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/");
		 * map = JSON2HashMap.convert(json);
		 * 
		 *  The code above should produce the map given below. 
		 */
		map.put("images.5.types.0", "Booklet");
		map.put("images.0.id", "11671794778");
		map.put("images.5.front", "false");
		map.put("images.0.edit", "35372447");
		map.put("release", "http://musicbrainz.org/release/96c2c341-6a63-31c4-8453-1db79f881e78");
		map.put("images.4.id", "11671800228");
		map.put("images.0.thumbnails.small", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671794778-250.jpg");
		map.put("images.5.image", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671801214.jpg");
		map.put("images.3.types.0", "Tray");
		map.put("images.2.thumbnails.small", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671798341-250.jpg");
		map.put("images.1.front", "false");
		map.put("images.1.comment", "");
		map.put("images.3.approved", "true");
		map.put("images.2.image", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671798341.jpg");
		map.put("images.4.back", "false");
		map.put("images.5.edit", "35372453");
		map.put("images.1.thumbnails.small", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671796214-250.jpg");
		map.put("images.0.approved", "true");
		map.put("images.3.back", "false");
		map.put("images.3.id", "11671799303");
		map.put("images.1.thumbnails.large", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671796214-500.jpg");
		map.put("images.4.front", "false");
		map.put("images.0.back", "false");
		map.put("images.3.thumbnails.small", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671799303-250.jpg");
		map.put("images.1.image", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671796214.jpg");
		map.put("images.4.comment", "");
		map.put("images.4.image", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671800228.jpg");
		map.put("images.1.edit", "35372448");
		map.put("images.2.approved", "true");
		map.put("images.4.edit", "35372452");
		map.put("images.1.approved", "true");
		map.put("images.4.thumbnails.large", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671800228-500.jpg");
		map.put("images.0.types.0", "Front");
		map.put("images.0.image", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671794778.jpg");
		map.put("images.3.edit", "35372451");
		map.put("images.4.types.0", "Back");
		map.put("images.4.types.1", "Spine");
		map.put("images.2.id", "11671798341");
		map.put("images.2.types.0", "Medium");
		map.put("images.0.front", "true");
		map.put("images.3.front", "false");
		map.put("images.3.thumbnails.large", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671799303-500.jpg");
		map.put("images.2.comment", "");
		map.put("images.1.back", "true");
		map.put("images.2.edit", "35372449");
		map.put("images.5.thumbnails.small", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671801214-250.jpg");
		map.put("images.0.comment", "");
		map.put("images.2.thumbnails.large", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671798341-500.jpg");
		map.put("images.1.id", "11671796214");
		map.put("images.5.approved", "true");
		map.put("images.5.id", "11671801214");
		map.put("images.3.comment", "");
		map.put("images.5.thumbnails.large", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671801214-500.jpg");
		map.put("images.3.image", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671799303.jpg");
		map.put("images.1.types.0", "Back");
		map.put("images.4.thumbnails.small", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671800228-250.jpg");
		map.put("images.5.back", "false");
		map.put("images.2.back", "false");
		map.put("images.0.thumbnails.large", "http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671794778-500.jpg");
		map.put("images.2.front", "false");
		map.put("images.4.approved", "true");
		map.put("images.5.comment", ", value)");
	}

	
	@Test
	public void testFrontCoverFinder() {
		/*
		 * We are looking for the images that has front=true, we would prefer images.x.thumbnails.large. 
		 * If thumbnails.large isn't available we would like images.x.thumbnails.small. 
		 * If that doesn't exist either then go for images.x.image.  
		 */
		assertEquals("http://coverartarchive.org/release/96c2c341-6a63-31c4-8453-1db79f881e78/11671794778-500.jpg",
				FrontCoverFinder.getCoverURL(map));
	}

}
